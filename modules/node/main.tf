resource "aws_instance" "main" {
  ami                  = var.ami_id
  instance_type        = var.instance_type
  iam_instance_profile = aws_iam_instance_profile.main.name
  key_name             = var.ec2_key
  tags = {
    Name = var.name
  }
  vpc_security_group_ids = concat(
    [aws_security_group.main.id],
    var.security_groups
  )
}
