output "instance_id" {
  description = "Instance identifier"
  value       = aws_instance.main.id
}

output "node_sg_id" {
  description = "Instance security group identifier"
  value       = aws_security_group.main.id
}
