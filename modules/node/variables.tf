variable "ec2_key" {
  type        = string
  description = "EC2 key name"
}

variable "vpc_id" {
  type        = string
  description = "VPC identifier"
}

variable "subnet_id" {
  type        = string
  description = "Subnet identifier"
}

variable "ami_id" {
  type        = string
  description = "AMI identifier"
}

variable "name" {
  type        = string
  description = "Instance name"
  default     = "umami-node"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t3.nano"
}

variable "security_groups" {
  type        = list(string)
  description = "Additional security groups"
  default     = []
}

variable "tags" {
  type        = map(any)
  description = "Resource tags"
  default     = {}
}
