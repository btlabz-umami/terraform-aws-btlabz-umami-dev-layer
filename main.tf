# EC2 Key Pair
resource "aws_key_pair" "main" {
  key_name   = "umami-dev-key"
  public_key = var.public_key
  tags = merge(
    var.tags,
    { Name = "umami-key" }
  )
}

# Dev node 01
module "node_01" {
  name            = "umami-dev-node-01"
  source          = "./modules/node"
  vpc_id          = var.vpc_id
  subnet_id       = var.subnet_id
  instance_type   = "t3.small"
  ami_id          = var.ami_id
  ec2_key         = aws_key_pair.main.key_name
  security_groups = [aws_security_group.main.id]
  tags            = var.tags
}

/*
module "node_02" {
  name          = "umami-dev-node-02"
  source        = "./modules/node"
  vpc_id = var.vpc_id
  subnet_id     = var.subnet_id
  instance_type = "t3.small"
  ami_id          = var.ami_id
  ec2_key         = aws_key_pair.main.key_name
  security_groups = [aws_security_group.main.id]
  tags            = var.tags
}
*/

/*
module "node_03" {
  name          = "umami-dev-node-03"
  source        = "./modules/node"
  vpc_id = var.vpc_id
  subnet_id     = var.subnet_id
  instance_type = "t3.small"
  ami_id          = var.ami_id
  ec2_key         = aws_key_pair.main.key_name
  security_groups = [aws_security_group.main.id]
  tags            = var.tags
}
*/
