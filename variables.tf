variable "vpc_id" {
  type        = string
  description = ""
}

variable "subnet_id" {
  type        = string
  description = ""
}

variable "ami_id" {
  type        = string
  description = ""
}

variable "ops_access" {
  type        = list(string)
  description = ""
  default     = ["0.0.0.0/0"]
}

variable "cli_access" {
  type        = list(string)
  description = ""
  default     = ["0.0.0.0/0"]
}

variable "public_key" {
  type        = string
  description = "SSH public key"
}

variable "tags" {
  type        = map(any)
  description = "Infrastructure tags"
  default     = {}
}
