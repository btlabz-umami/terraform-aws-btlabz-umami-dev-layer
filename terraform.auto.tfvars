ami_id        = "ami-04e905a52ec8010b2"
instance_type = "t3.small"

tags = {
  project   = "umami"
  env       = "dev"
  terraform = "true"
}