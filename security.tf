resource "aws_security_group" "main" {
  name        = "umami-dev-sg"
  description = "umami-dev-sg"
  vpc_id      = "vpc-9138bcf8"
  tags = merge(
    var.tags,
    { Name = "umami-dev-sg" }
  )
}

resource "aws_security_group_rule" "egress" {
  type              = "egress"
  description       = "allow all"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.main.id
}

resource "aws_security_group_rule" "control" {
  type              = "ingress"
  description       = "allow ssh"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = var.ops_access
  security_group_id = aws_security_group.main.id
}

resource "aws_security_group_rule" "service" {
  type              = "ingress"
  description       = "allow service internals"
  from_port         = 3000
  to_port           = 3000
  protocol          = "tcp"
  cidr_blocks       = var.ops_access
  security_group_id = aws_security_group.main.id
}

resource "aws_security_group_rule" "proxy_http" {
  type              = "ingress"
  description       = "allow http"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = var.cli_access
  security_group_id = aws_security_group.main.id
}

resource "aws_security_group_rule" "proxy_https" {
  type              = "ingress"
  description       = "allow https"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = var.cli_access
  security_group_id = aws_security_group.main.id
}
