# terraform-aws-btlabz-umami-dev-layer

## Overview

This is a sample layer to deploy umami ec2 instance. Some fetaures included:

- security groups for ops and cli
- iam role with ssm access
- ec2 key deployment

## Requirements

- terraform 1.21.1
- aws account, enabled paris region
- tfcloud account

## Runbook

### Prerequisites

Deploy VPC in eu-west-3 and fetch vpc-id and subnet-id.
You can use default VPC, or you can use a [terraform module](https://registry.terraform.io/modules/btower-labz/btlabz-vpc-ha-3x/aws/latest) to deploy one.

Deside on access restrictions. By default module allows 0.0.0.0/32 to access the service and control the node.
You can restrict the access to dev machine only. It' possible to get external ip like this:

```bash
curl https://checkip.amazonaws.com
```

### Deploy

```bash
git clone https://gitlab.com/btlabz-umami/terraform-aws-btlabz-umami-dev-layer.git
cd terraform-aws-btlabz-umami-dev-layer
```

Configure environment variables. E.g. create terraform.secret.auto.tfvars with actual content. Use gathered information.

```hcl
vpc_id     = "vpc-9***8"
subnet_id  = "subnet-3***5"
public_key = "ssh-ed25519 AAA***dS ops@wks"
ops_access = ["0.0.0.0/0"]
cli_access = ["0.0.0.0/0"]
```

Remove or reconfigure tfcloud backend: [backend.tf](backend.tf)

Plan and apply.

```bash
terraform plan
terraform apply
```

Configure node with ansible playbook: [btlabz-ansible-umami-playbook](https://gitlab.com/btlabz-umami/btlabz-ansible-umami-playbook)

## TODOs

- separate node module
- more comments and descriptions

## References

- [T1: apache log analisys with python, bash, sqlite and docker](https://gitlab.com/btlabz-other/btlabz-docker-httpd2sqlite-poc)
- [T2: terraform node](https://gitlab.com/btlabz-umami/terraform-aws-btlabz-umami-dev-layer) and [ansible playbook](https://gitlab.com/btlabz-umami/btlabz-ansible-umami-playbook)
- [T3: Umami HA and resilience considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-ha)
- [T4: Umami monitoring considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-monitoring)
